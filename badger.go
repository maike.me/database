package database

import (
	"time"

	"github.com/dgraph-io/badger"
)

type Badger struct {
	badger *badger.DB
}

func OpenBadger(opts badger.Options) (KV, error) {
	kv, err := badger.Open(opts)
	if err != nil {
		return nil, err
	}
	return &Badger{kv}, err
}

func (db *Badger) GCTicker(discardRatio float64, duration time.Duration) {
	ticker := time.NewTicker(time.Second * 1)
	stop := make(chan bool, 1)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			err := db.badger.RunValueLogGC(discardRatio)
			switch err {
			case badger.ErrRejected:
				stop <- true
			}
		case <-stop:
			return
		}
	}
}

func (db *Badger) Set(key, value []byte) error {
	return db.badger.Update(func(txn *badger.Txn) error {
		return txn.Set(key, value)
	})
}

func (db *Badger) SetWithTTL(key, value []byte, duration time.Duration) error {
	return db.badger.Update(func(txn *badger.Txn) error {
		return txn.SetWithTTL(key, value, duration)
	})
}

func (db *Badger) Get(key []byte) ([]byte, error) {
	var value []byte
	err := db.badger.View(func(txn *badger.Txn) error {
		item, err := txn.Get(key)
		if err != nil {
			return err
		}
		value, err = item.Value()
		return err
	})
	return value, err
}

func (db *Badger) Delete(key []byte) error {
	return db.badger.Update(func(txn *badger.Txn) error {
		return txn.Delete(key)
	})
}

func (db *Badger) Close() error {
	return db.badger.Close()
}
