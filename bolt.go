package database

import (
	"time"

	bolt "go.etcd.io/bbolt"
)

type Bolt struct {
	bolt *bolt.DB
}

var boltBucket []byte

func OpenBolt(path string, bucket []byte, options bolt.Options) (KV, error) {
	kv, err := bolt.Open(path, 0666, &options)
	if err != nil {
		return nil, err
	}
	boltBucket = bucket
	if err = kv.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(boltBucket)
		return err
	}); err != nil {
		kv.Close()
		return nil, err
	}
	return &Bolt{kv}, nil
}

func (db *Bolt) Set(key []byte, value []byte) error {
	return db.bolt.Update(func(tx *bolt.Tx) error {
		return tx.Bucket(boltBucket).Put(key, value)
	})
}

func (db *Bolt) Get(key []byte) ([]byte, error) {
	var value []byte
	err := db.bolt.View(func(tx *bolt.Tx) error {
		value = tx.Bucket(boltBucket).Get(key)
		return nil
	})
	return value, err
}

func (db *Bolt) Delete(key []byte) error {
	return db.bolt.Update(func(tx *bolt.Tx) error {
		return tx.Bucket(boltBucket).Delete(key)
	})
}

func (db *Bolt) Close() error {
	return db.bolt.Close()
}

func (db *Bolt) SetWithTTL(key, value []byte, duration time.Duration) error { return nil }
func (db *Bolt) GCTicker(discardRatio float64, duration time.Duration)      {}
