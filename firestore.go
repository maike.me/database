package database

import (
	"context"

	"cloud.google.com/go/firestore"
)

type FireStore struct {
	Client *firestore.Client
}

func OpenFireStore(ctx context.Context, projectID string) (NoSQL, error) {
	client, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		return nil, err
	}
	return &FireStore{
		Client: client,
	}, nil
}

func (db *FireStore) FireStoreClient() *firestore.Client {
	return db.Client
}

func (db *FireStore) Add(ctx context.Context, collection string, data map[string]interface{}) error {
	if _, _, err := db.Client.Collection(collection).Add(ctx, data); err != nil {
		return err
	}
	return nil
}

func (db *FireStore) Set(ctx context.Context, collection, doc string, data map[string]interface{}) error {
	if _, err := db.Client.Collection(collection).Doc(doc).Set(ctx, data); err != nil {
		return err
	}
	return nil
}

func (db *FireStore) Delete(ctx context.Context, collection, doc string) error {
	if _, err := db.Client.Collection(collection).Doc(doc).Delete(ctx); err != nil {
		return err
	}
	return nil
}

func (db *FireStore) DeleteField(ctx context.Context, collection, doc, path string) error {
	if _, err := db.Client.Collection(collection).Doc(doc).Update(ctx, []firestore.Update{
		{
			Path:  path,
			Value: firestore.Delete,
		},
	}); err != nil {
		return err
	}
	return nil
}
