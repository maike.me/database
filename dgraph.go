package database

import (
	"context"
	"encoding/json"

	"github.com/buger/jsonparser"
	"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/protos/api"
	"google.golang.org/grpc"
)

type Dgraph struct {
	Client *dgo.Dgraph
	Conn   *grpc.ClientConn
}

func OpenDgraph(addr string) (Graph, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	client := dgo.NewDgraphClient(api.NewDgraphClient(conn))
	return &Dgraph{
		Client: client,
		Conn:   conn,
	}, nil
}

func (db *Dgraph) GraphClient() *dgo.Dgraph {
	return db.Client
}

func (db *Dgraph) SetSchema(schema string) error {
	return db.Client.Alter(context.Background(), &api.Operation{
		Schema: schema,
	})
}

func (db *Dgraph) SetJSON(data []byte) (*api.Assigned, error) {
	mut := &api.Mutation{
		CommitNow: true,
		SetJson:   data,
	}
	return db.Client.NewTxn().Mutate(context.Background(), mut)
}

func (db *Dgraph) SetInterface(data interface{}) (*api.Assigned, error) {
	plainJSON, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	mut := &api.Mutation{
		CommitNow: true,
		SetJson:   plainJSON,
	}
	return db.Client.NewTxn().Mutate(context.Background(), mut)
}

func (db *Dgraph) DeleteJSON(data []byte) (*api.Assigned, error) {
	mut := &api.Mutation{
		CommitNow:  true,
		DeleteJson: data,
	}
	return db.Client.NewTxn().Mutate(context.Background(), mut)
}

func (db *Dgraph) DeleteInterface(data interface{}) (*api.Assigned, error) {
	plainJSON, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	mut := &api.Mutation{
		CommitNow:  true,
		DeleteJson: plainJSON,
	}
	return db.Client.NewTxn().Mutate(context.Background(), mut)
}

func (db *Dgraph) Query(query string) (*api.Response, error) {
	return db.Client.NewTxn().Query(context.Background(), query)
}

func (db *Dgraph) QueryWithVars(query string, vars map[string]string) (*api.Response, error) {
	return db.Client.NewTxn().QueryWithVars(context.Background(), query, vars)
}

func (db *Dgraph) NodeValueExists(node, value string) bool {
	resp, err := db.Client.NewTxn().Query(context.Background(), `{
		exists(func: eq(`+node+`, "`+value+`")) {
			uid
		}
	}`)
	if err != nil {
		return false
	}
	if _, err = jsonparser.GetString(resp.GetJson(), "exists", "[0]", "uid"); err != nil {
		return false
	}
	return true
}
