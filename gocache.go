package database

import (
	"time"

	gocache "github.com/patrickmn/go-cache"
)

type GoCache struct {
	gocache *gocache.Cache
}

func OpenGoCache(defaultExpiration, cleanupInterval time.Duration) Cache {
	return &GoCache{gocache.New(defaultExpiration, cleanupInterval)}
}

func (cache *GoCache) Get(key string) (interface{}, bool) {
	return cache.gocache.Get(key)
}

func (cache *GoCache) Set(key string, value interface{}, duration time.Duration) {
	cache.gocache.Set(key, value, duration)
}
