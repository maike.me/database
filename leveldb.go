package database

import (
	"time"

	"github.com/syndtr/goleveldb/leveldb"
)

type LevelDB struct {
	leveldb *leveldb.DB
}

func OpenLevelDB(path string) (KV, error) {
	kv, err := leveldb.OpenFile(path, nil)
	if err != nil {
		return nil, err
	}
	return &LevelDB{kv}, nil
}

func (db *LevelDB) Set(key, value []byte) error {
	return db.leveldb.Put(key, value, nil)
}

func (db *LevelDB) Get(key []byte) ([]byte, error) {
	return db.leveldb.Get(key, nil)
}

func (db *LevelDB) Delete(key []byte) error {
	return db.leveldb.Delete(key, nil)
}

func (db *LevelDB) Close() error {
	return db.leveldb.Close()
}

func (db *LevelDB) SetWithTTL(key, value []byte, duration time.Duration) error { return nil }
func (db *LevelDB) GCTicker(discardRatio float64, duration time.Duration)      {}
